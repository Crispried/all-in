# Login
sudo docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY

# Build the project 
sudo docker build -t builder -f BuilderDockerfile .

# Build the deployment containers
sudo docker build -t $CI_REGISTRY_IMAGE/frontend:dev -f FrontendDockerfile . &
wait

# Push the new images
sudo docker push $CI_REGISTRY_IMAGE/frontend:dev &
wait
