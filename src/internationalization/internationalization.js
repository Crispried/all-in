import Vue from 'vue'
import VueI18n from 'vue-i18n'
import pl from './pl.js'
import en from './en.js'

Vue.use(VueI18n)

export function createI18n () {
  return new VueI18n({
    locale: 'pl',
    messages: {
      'pl': pl,
      'en': en
    }
  })
}