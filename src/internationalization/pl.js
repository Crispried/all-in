export default {
  content: {
    title: "Najlepsze ROZWIĄZANIE <br> dla twojego biznesu",
    tabletTitle: "Najlepsze<br> ROZWIĄZANIE <br> dla twojego <br> biznesu",
    desc: "Odpowiedni personel pod indywidualne <br> zapotrzebowanie klientów",
    tabletDesc: "Odpowiedni personel pod<br> indywidualne zapotrzebowanie klientów",
    mobileDesc: "Odpowiedni personel pod<br> indywidualne zapotrzebowanie <br> klientów",
    services: [
      {
        id: 1,
        imgLink: require("../assets/img/service1.svg"),
        subText: 'Outsourcing pracowniczy',
      },
      {
        id: 2,
        imgLink: require("../assets/img/service2.svg"),
        subText: 'Doradztwo i rozwiązania HR'
      },
      {
        id: 3,
        imgLink: require("../assets/img/service3.svg"),
        subText: 'Outsourcing IT'
      }
    ],
    informationTitle: "Wskaźniki <br> naszej firmy",
    informationTitleTablet: "Wskaźniki naszej firmy",
    informationList: [
      {
        id: 1,
        text: "Więcej 50 zadowolonych <br> ze współpracy klientów"
      },
      {
        id: 2,
        text: "Około 600 zatrudnionych <br> pracowników"
      },
      {
        id: 3,
        text: "5 lat doświadczenia <br> w branży HR"
      }
    ],
    informationListTablet: [
      {
        id: 1,
        text: "Więcej 50 zadowolonych ze współpracy klientów"
      },
      {
        id: 2,
        text: "Około 600 zatrudnionych pracowników"
      },
      {
        id: 3,
        text: "5 lat doświadczenia w branży HR"
      }
    ],
    informationListMobile: [
      {
        id: 1,
        text: "Więcej 50 zadowolonych <br> ze współpracy klientów"
      },
      {
        id: 2,
        text: "Około 600 zatrudnionych <br> pracowników"
      },
      {
        id: 3,
        text: "5 lat doświadczenia <br> w branży HR"
      }
    ],
    advantages: [
      {
        id: 1,
        iconURL: require('../assets/img/advantages/1.svg'),
        text: "Rekrutujemy doświadczonych<br>" +
          "specjalistów różnych branży<br>" +
          "przez nasze biura w Rosji,<br>" +
          "Ukrainie i Białorusi",
        color: "#6689ff"
      },
      {
        id: 2,
        iconURL: require('../assets/img/advantages/2.svg'),
        text: "Przygotowujemy wszystkie<br>" +
          "dokumenty niezbędny<br>" +
          "do legalnego przystąpienia<br>" +
          "do pracy",
        color: "#f78469"
      },
      {
        id: 3,
        iconURL: require('../assets/img/advantages/3.svg'),
        text: "Zapewniamy pracownikom<br>" +
          "mieszkania w bliskiej do pracy<br>" +
          "lokalizacji",
        color: "#f78469"
      },
      {
        id: 4,
        iconURL: require('../assets/img/advantages/4.svg'),
        text: "Na żądanie klientów<br>" +
          "oferujemy dojazd pracowników<br>" +
          "do miejsca pracy",
        color: "#16ccbb"
      },
      {
        id: 5,
        iconURL: require('../assets/img/advantages/5.svg'),
        text: "Zbiorcza faktura<br>" +
          "co miesiąc za wszystkich<br>" +
          "pracowników",
        color: "#6689ff"
      },
      {
        id: 6,
        iconURL: require('../assets/img/advantages/6.svg'),
        text: "Spełniamy nawet<br>" +
          "największe zapotrzebowanie<br>" +
          "w pracownikach nie więcej<br>" +
          "niż za 2 tygodni",
        color: "#2d2d57"
      },
      {
        id: 7,
        iconURL: require('../assets/img/advantages/7.svg'),
        text: "Rekrutujemy pracowników<br>" +
          "z krajów azjatyckich",
        color: "#16ccbb"
      }
    ],
    advantagesTablet: [
      {
        id: 1,
        iconURL: require('../assets/img/advantages/1.svg'),
        text: "Rekrutujemy doświadczonych<br>" +
          "specjalistów różnych branży<br>" +
          "przez nasze biura w Rosji,<br>" +
          "Ukrainie i Białorusi",
        color: "#6689ff"
      },
      {
        id: 2,
        iconURL: require('../assets/img/advantages/2.svg'),
        text: "Przygotowujemy wszystkie<br>" +
          "dokumenty niezbędny<br>" +
          "do legalnego przystąpienia<br>" +
          "do pracy",
        color: "#f78469"
      },
      {
        id: 5,
        iconURL: require('../assets/img/advantages/5.svg'),
        text: "Zbiorcza faktura<br>" +
          "co miesiąc za wszystkich<br>" +
          "pracowników",
        color: "#6689ff"
      },
      {
        id: 3,
        iconURL: require('../assets/img/advantages/3.svg'),
        text: "Zapewniamy pracownikom<br>" +
          "mieszkania w bliskiej do pracy<br>" +
          "lokalizacji",
        color: "#f78469"
      },
      {
        id: 4,
        iconURL: require('../assets/img/advantages/4.svg'),
        text: "Na żądanie klientów<br>" +
          "oferujemy dojazd pracowników<br>" +
          "do miejsca pracy",
        color: "#16ccbb"
      },
      {
        id: 6,
        iconURL: require('../assets/img/advantages/6.svg'),
        text: "Spełniamy nawet<br>" +
          "największe zapotrzebowanie<br>" +
          "w pracownikach nie więcej<br>" +
          "niż za 2 tygodni",
        color: "#2d2d57"
      },
      {
        id: 7,
        iconURL: require('../assets/img/advantages/7.svg'),
        text: "Rekrutujemy pracowników<br>" +
          "z krajów azjatyckich",
        color: "#16ccbb"
      }
    ],
    advantagesMobile: [      
      {
        id: 2,
        iconURL: require('../assets/img/advantages/2.svg'),
        text: "Przygotowujemy wszystkie<br>" +
          "dokumenty niezbędny<br>" +
          "do legalnego przystąpienia<br>" +
          "do pracy",
        color: "#f78469"
      },
      {
        id: 3,
        iconURL: require('../assets/img/advantages/3.svg'),
        text: "Zapewniamy pracownikom<br>" +
          "mieszkania w bliskiej do pracy<br>" +
          "lokalizacji",
        color: "#f78469"
      },
      {
        id: 1,
        iconURL: require('../assets/img/advantages/1.svg'),
        text: "Rekrutujemy doświadczonych<br>" +
          "specjalistów różnych branży<br>" +
          "przez nasze biura w Rosji,<br>" +
          "Ukrainie i Białorusi",
        color: "#6689ff"
      },
      {
        id: 5,
        iconURL: require('../assets/img/advantages/5.svg'),
        text: "Zbiorcza faktura<br>" +
          "co miesiąc za wszystkich<br>" +
          "pracowników",
        color: "#6689ff"
      },
      {
        id: 4,
        iconURL: require('../assets/img/advantages/4.svg'),
        text: "Na żądanie klientów<br>" +
          "oferujemy dojazd pracowników<br>" +
          "do miejsca pracy",
        color: "#16ccbb"
      },
      {
        id: 6,
        iconURL: require('../assets/img/advantages/6.svg'),
        text: "Spełniamy nawet<br>" +
          "największe zapotrzebowanie<br>" +
          "w pracownikach nie więcej<br>" +
          "niż za 2 tygodni",
        color: "#2d2d57"
      },
      {
        id: 7,
        iconURL: require('../assets/img/advantages/7.svg'),
        text: "Rekrutujemy pracowników<br>" +
          "z krajów azjatyckich",
        color: "#16ccbb"
      }
    ],
    reviewTitle: "Opinie klientów",
    reviewImg: require('../assets/img/review-img.svg'),
    reviews: [
      {
        key: 1,
        infoText: "Zachęcam z pełną odpowiedziałnością do podejmowania współpracy z „ALL-IN” z Poznania w zakresie: rekrutacji oraz pracowników tymczasowych. Profesjonalna obsługa, szybkość działania jak i dostępność na miejscu koordynatorów agencji – przekładało się na bezproblemowy przebieg naszej współpracy. ",
        companyName: "“Wirenka”",
        companyLogo: require('../assets/img/wirenka-logo.png')
      },
      {
        key: 2,
        infoText: "Kontakt z firmą należy do udanych ze względu na pewną jakość usługi. Przedstawiciele przedsiębiorstwa dbają o najlepszy standard działania, co jest głównym z ich atutów.  Firma stara się o spełnić wymagania zleceniodawcy, dlatego poświęca wiele uwagi realizacji umowy. Stabilność wynikająca ze wzbudzanego przez firmę poczucia bezpieczeństwa niezwykłe pozytywnie wpływa na ogólną jakość kooperacji. ",
        companyName: "“Conor Capital”",
        companyLogo: require('../assets/img/conor_capital-logo.jpg')
      },
      {
        key: 3,
        infoText: "Współpracujemy z „ALL-IN” już od 2 lat. W ciągu współpracy zlecono było więcej 50 osób. Współpraca z „ALL-IN” wykazała się tylko z najlepszej strony, a spełnienie warunków umowy i poważne traktowanie zleceniodawcy przez pracowników „ALL-IN” pokazuje wysoki poziom świadczenia usług. Zachęcam do współpracy z „ALL-IN” z pełną odpowiedzialnością.",
        companyName: "“Constant”",
        companyLogo: require('../assets/img/pooldream_lux-logo.png')
      }
    ],
    contactTitle: "Contact us"
  },
  footerContent: {
    adress: "ul. Przemysłowa 39 <br> 60-101 Poznań, Polska",
    logoURL: require('../assets/img/logo.svg'),
    sertificatsText: "Certyfikaty firmowe",
    sertificatsLink: "https://drive.google.com/drive/folders/1MfRcx2_IoSHBiP2BJd3nIisIALnio4_2",
    contacts: [{
      iconURL: require('../assets/img/contact_icon1.svg'),
      text: "ul. Przemysłowa 39 <br> 60-101 Poznań, Polska"
    },
    {
      iconURL: require('../assets/img/contact_icon2.svg'),
      text: "+48 727 889 247 <br>" +
        "+48 796 677 475"
    },
    {
      iconURL: require('../assets/img/contact_icon3.svg'),
      text: "info.rekrutacja.ua@gmail.com",
      textMobile: "info.rekrutacja.ua@gmail.com"
    }
    ],
    socialNetworks: [{
      imgURL: require('../assets/img/fb.svg'),
      link: "https://facebook.com",
      alt: "facebook"
    },
    {
      imgURL: require('../assets/img/vk.svg'),
      link: "https://vk.com",
      alt: "vkontakte"
    },
    ],
  }
}