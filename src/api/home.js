import { api } from './api';

export default {
  sendForm(sendObj) {
    return api().post('api/home/contact', sendObj)
      .then(response => response.json())
      .then(json => console.log(json))
      .catch(e => {
        console.log(e);
      })
  }
}