export default {
    out: null,
    isDesktop(width) {
        this.out = (width > 960) ? true : false;
        return this.out;
    },
    isTablet(width) {
        this.out = (width < 960 && width > 600) ? true : false;
        return this.out;
    },
    isMobile(width) {
        this.out = (width < 600) ? true : false;
        return this.out;
    }
}
