import Vue from 'vue'
import App from './App.vue'
import 'normalize.css'
import common from './common.js'
import axios from 'axios'
import api from './api/api.js'
import homeValidation from './validation/home-validation.js'
import * as VueGoogleMaps from 'vue2-google-maps'
import 'slick-carousel/slick/slick.css';
import slick from 'slick-carousel'
import { createI18n } from './internationalization/internationalization.js'
Vue.config.productionTip = false
Vue.prototype.axios = axios
Vue.prototype.api = api
Vue.prototype.common = common
Vue.prototype.homeValidation = homeValidation
const i18n = createI18n();
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyB4KDkncrxyJ0Lz4fBFMJHcPEWsFkdVO1g',
 // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
 
    //// If you want to set the version, you can do so:
    // v: '3.26',
}})
new Vue({
  i18n,
  render: h => h(App)
}).$mount('#app')

