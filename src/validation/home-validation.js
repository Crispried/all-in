export default (model) => {
  function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  if (model.type == 'text') {
    if (model.out == "") {
      model.valid = false;
      model.validText = "*Wymagane pole";
    } else {
      model.valid = true;
    }
  }
  if (model.type == 'phone') {
    var phoneRegexp = /^\+(\d{2}( |-)?)?(\(?[0-9]{2}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$/
    if (model.out == "") {
      model.valid = false;
      model.validText = "*Wymagane pole";
    } else if (!phoneRegexp.test(model.out)) {
      model.valid = false;
      model.validText = "*Wprowadź prawidłowy numer telefonu";
    } else {
      model.valid = true;
    }
  }
  if (model.type == 'email') {
    if (model.out == "") {
      model.valid = false;
      model.validText = "*Wymagane pole";
    } else if (model.out.indexOf('@') == '-1') {
      model.valid = false;
      model.validText = "*Podaj prawidłowy adres e-mail";
    } else if (!validateEmail(model.out)) {
      model.valid = false;
      model.validText = "*Podaj prawidłowy adres e-mail";
    } else {
      model.valid = true;
    }
  }
  if (model.type == 'number') {
    if (model.out.length == 0) {
      model.valid = false;
      model.validText = "*Wymagane pole";
    } else if (model.out != parseInt(model.out) && model.out.length != 0) {
      model.valid = false;
      model.validText = "*To pole może zawierać tylko cyfry";
    } else {
      model.valid = true;
    }
  }
  return model;
}