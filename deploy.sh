# Login
sudo docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY

# Pull the images
sudo docker pull $CI_REGISTRY_IMAGE/frontend:dev

# Remove the old containers
sudo docker stop frontend || true && sudo docker rm frontend -f || true

# Cleanup the old images
sudo docker rmi `sudo docker images -aq`

# Free the storage
sudo docker rmi -f $(sudo docker images -aq --filter dangling=true)

# Run the containers
sudo docker run --name=frontend -p 1111:1111 -itd $CI_REGISTRY_IMAGE/frontend:dev
